import React from 'react'
import ReactTable from 'react-table'
import validateCurrencyCode from 'validate-currency-code'
import 'react-table/react-table.css'

const DroneTable = ({ drones }) => {
  const columns = [
    {Header: 'Name', accessor: 'name', Cell: ({value}) => <span>{value}{value.match(/potato/i) && '*'}</span>},
    {Header: 'Number of Flights', accessor: 'numFlights'},
    {Header: 'Number of Crashes', accessor: 'numCrashes'},
    {Header: 'Crash %', id: 'crashperc', accessor: d => d, Cell: ({value: {numFlights, numCrashes}}) => numFlights ? percentage(numCrashes / numFlights) : '-'},
    {Header: 'Price', id: 'price', accessor: d => d, Cell: ({value: {price, currency: currencyCode}}) => currency(price, currencyCode)},
  ]
  return <div>
    <ReactTable
      data={drones}
      columns={columns}
      filterable
    />
    <div><small><i>*All potato drones come with free slow clap processor!</i></small></div>
  </div>
}

const percentage = (n, maximumFractionDigits=2) => new Intl.NumberFormat('en', { style: 'percent', maximumFractionDigits}).format(n)

const currency = (price, currency) => validateCurrencyCode(currency)
  ? new Intl.NumberFormat('en', { style: 'currency', currency }).format(price)
  : `${price} ${currency}`

export default DroneTable
