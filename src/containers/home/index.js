import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { actions as droneActions } from '../../modules/drones'
import DroneTable from './DroneTable'

class Home extends React.Component {
  componentDidMount = () => this.props.fetchDrones()
  render = () => <div>
    <h1>Bob's Epic Drone Shack</h1>
    {this.props.loading
      ? <div>Please wait while the table loads...</div>
      : <DroneTable drones={this.props.drones} />
    }
  </div>
}

const mapStateToProps = (state) => ({
  drones: state.drones.drones || [],
  loading: state.drones.loading,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      ...droneActions,
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
