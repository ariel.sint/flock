const reliableApi = (url, body, method='GET') => {
  return new Promise((resolve, reject) => {
    let updatedURL = new URL('https://bobs-epic-drone-shack-inc.herokuapp.com/api/v0/' + url)

    // 410 means *permanantly* gone so we need to stop the browser caching this
    updatedURL.searchParams.set('ts', +(new Date()))
    
    fetch(updatedURL, {method, body})
      .then((request) => {
	if (request.ok) {
	  return request.json()
	} else throw Error(`${request.status}: ${request.statusText}`)
      })
      .then(resolve)
      .catch((err) => resolve(reliableApi(url, body, method)))
  })
}

export default reliableApi
