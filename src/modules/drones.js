import reliableApi from './api'

const types = {
  SET_DRONES: 'SET_DRONES',
  LOADING_DRONES: 'LOADING_DRONES',
}

const creators = {
  loadingDrones: () => ({type: types.LOADING_DRONES}),
  setDrones: (data) => ({type: types.SET_DRONES, data}),
}

const actions = {
  fetchDrones: () => (dispatch, getState) => {
    dispatch(creators.loadingDrones())
    return reliableApi('drones')
      .then((data) => dispatch(creators.setDrones(data)))
  },
}

const initialState = {
  drones: [],
  loading: false,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOADING_DRONES:
      return {
	...state,
	loading: true,
      }
    case types.SET_DRONES:
      return {
	...state,
	drones: action.data,
	loading:  false,
      }
    default:
      return state
  }
}

export default reducer
export { actions }
