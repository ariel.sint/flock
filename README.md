## Installation

```bash
git clone git@gitlab.com:ariel.sint/flock.git ariel-flock
cd ariel-flock
yarn
```

## Get started

```bash
yarn start
```

## Key assumptions

- Probably the biggest assumption I made is that Bob would be ok without any custom rate limiting. If the reliableApi function fails, it will immediately recurse with no delay. It is left entirely to the browser to rate limit subsequent requests.

- Additionally, I assume that the API would never fail enough times to cause a call stack overflow.

- I also assume that all potato based drones come with a slow clap processor, [because I'm a potato](https://youtu.be/N4XAUdxfF3Q).

## Compromises

As part of developing this entirely on a phone, a number of compromises were made.

- Firstly there is no css preprocessor. Not all of the dependencies compiled on my phone, so I had to drop it. Rectification would be through installing it with npm, along with css framworks such as bootstrap and/or tachyon, and then adding appropriate classes to the html.

- Additionally, with no console or network tab, inspecting headers proved tricky, so I opted to use a timestamp query parameter rather than no-cache headers in order to prevent caching. Not caching is important, as some of the error messages from the API are permanent which the browser will by default cache. Ideally we bypass this cache by telling the browser with a header, but currently the query parameter `ts` was set to the current timestamp to provide a unique, uncached URL.

## Future features

Although there is textual filtering (though this doesn't work for the crash percentage column) on the table from react-table, there is no range filtering. I do not know of any premade table that has range filtering from last time I looked, so the route that I would opt to implement this feature would be to firstly install a react range selector from npm. One that gives a minimum and a maximum would allow this component to be used to filter the rows in the data on the table, such that it only showed those with a crash rate within the range.
